## R Shiny module EditableDT

A R Shiny module to make editable DataTable (DT).  

Select columns to edit, add remove button, add button to add a default row.    
Select columns to show and tooltips.   

### Example

ui.R : 

```
editableDTUI("mytable")
```

server.R : 

```
myTable <- callModule(editableDT, id = "mytable", DTdata = my_data_frame, disableCol = c("colomns","not","editable"))
observeEvent(myTable$data, { message("Update my Table") })
```

### Parameters

Server Part

* DTdata : a reactive data.frame
* disableCol : reactiveVal for colnames not editable
* canRM : reactiveVal TRUE add delete button otherwise not
* rownames : TRUE if show rownames FALSE otherwise
* tooltips : vector for header tooltips message
* row.default : a default row (when adding a new line) depending of row.cols if not all cols existing
* row.colsid : list of columns that apply row.default
* row.inc : list of columns to incremente value at new line
* col.hidden : index of columns to hide.


### Authors

Jean-François Rey
